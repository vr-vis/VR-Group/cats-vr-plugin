# CATS VR Plugin

This plugin is intended to be used for the Powerwall at the [CATS](https://www.cats.rwth-aachen.de/go/id/nohs/) in tandem with the [RWTH VR Toolkit](https://git-ce.rwth-aachen.de/vr-vis/VR-Group/unreal-development/plugins/rwth-vr-toolkit) and the [Unreal DTrack Plugin](https://github.com/VRGroupRWTH/UnrealDTrackPlugin).

You just place the `BP_PowerwallSetup` actor in your scene to enable the correct camera setup and tracking. It is a modified version of the `BP_CaveSetup` actor from the VR Toolkit.
